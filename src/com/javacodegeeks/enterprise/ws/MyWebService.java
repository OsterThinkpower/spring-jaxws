package com.javacodegeeks.enterprise.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

import org.springframework.stereotype.Component;

import com.concretepage.soap.GetStudentRequest;
import com.concretepage.soap.GetStudentResponse;
import com.concretepage.soap.Student;

@Component("myWebService")
@WebService(serviceName="StudentService")
@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL, parameterStyle = ParameterStyle.BARE)
public class MyWebService {

	@WebMethod(operationName="getStudent", action="query")
	public GetStudentResponse printMessage(@WebParam(name="getStudentRequest") GetStudentRequest request) {
		Student s = new Student(); {
			s.setAge(23);
			s.setClazz("B");
			s.setName("Oster");
			s.setStudentId(1782);
		}
		GetStudentResponse response = new GetStudentResponse();
		response.setStudent(s);
		
		return response;

	}
}