package controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	@RequestMapping("/test") 
	public void test() {
		System.out.println("This is controller accept the request.");
	}
}
