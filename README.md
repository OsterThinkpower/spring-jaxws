# JAX-WS for Spring #

使用JAX-WS規則，並注入Spring容器。

### Web Service 簡易版 ###

* Server端
* Spring-MVC 與 JAX-WS 並行
* 主要Server程式 [/JAXWsWebService/src/com/javacodegeeks/enterprise/ws/MyWebService.java](https://bitbucket.org/OsterThinkpower/spring-jaxws/src/dc9aaad6e4536ce0ca36ddd49be1f18d8a26c334/src/com/javacodegeeks/enterprise/ws/MyWebService.java?at=master&fileviewer=file-view-default)
* 執行後取得WSDL
![螢幕快照 2016-04-28 下午3.44.52.png](https://bitbucket.org/repo/6kjLGM/images/165996860-%E8%9E%A2%E5%B9%95%E5%BF%AB%E7%85%A7%202016-04-28%20%E4%B8%8B%E5%8D%883.44.52.png)

### 設定方式 ###
* [web.xml](https://bitbucket.org/OsterThinkpower/spring-jaxws/src/dfa6d43a57e3403507a911385aa67c94523b9105/WebContent/WEB-INF/web.xml?at=master&fileviewer=file-view-default)
* [jaxws-spring-servlet.xml](https://bitbucket.org/OsterThinkpower/spring-jaxws/src/dfa6d43a57e3403507a911385aa67c94523b9105/WebContent/WEB-INF/jaxws-spring-servlet.xml?at=master&fileviewer=file-view-default)

### 參考 ###
[JAX WS with Spring](https://bthurley.wordpress.com/2014/04/27/web-services-with-jax-ws-jaxb-and-spring/)